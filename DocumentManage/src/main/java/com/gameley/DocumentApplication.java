package com.gameley;


import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@SpringBootApplication
@RestController
public class DocumentApplication {
    public static void main(String[] args) {
        SpringApplication.run(DocumentApplication.class, args);
    }

    @Autowired
    private TransportClient client;


    //增加接口
    @RequestMapping("/add")
    public String add() {
        try {
            XContentBuilder builder = XContentFactory.jsonBuilder().startObject()
                    .field("title", "123")
                    .field("author", "123")
                    .field("word_count", "123")
                    .field("public_date", "123")
                    .endObject();
            IndexResponse response = this.client.prepareIndex("book", "novel")
                    .setSource(builder).get();
            return "hello world";
        } catch (IOException e) {

            return "hello world";

        }

    }


}
